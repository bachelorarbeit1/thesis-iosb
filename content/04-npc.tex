\chapter{Nutzungskontrolle und Provenance Tracking in der Cloud}\label{ch:npc}

Die in Kapitel \ref{ch:b} und \ref{ch:cc} gesammelten Erkenntnisse sollen im Folgenden zur Ausbringung einer prototypischen Nutzungskontroll- sowie Provenance sammelnden Infrastruktur genutzt werden.

Konkret lauten die Ziele:
\begin{inparaenum}[(i)]
    \item Vereinfachung der Kommunikation zwischen den Komponenten der in \ref{sec:refucon} und \ref{sec:prov} beschriebenen Referenzarchitektur,
    \item Verbesserung der operativen Aspekte, insbesondere des Konfigurationsmanagements, sowie
    \item die Erforschung von Möglichkeiten zur automatisierten Bereitstellung einer solchen Infrastruktur.
\end{inparaenum}
Die Basis zur Erreichung dieser Ziele wird Kubernetes sein, das in Abschnitt \ref{sec:refkub} besprochen wird. Die darauffolgende Modellierung und Implementierung ist nicht statisch, sondern wird anhand unterschiedlicher Reifegrade bezüglich eines \textit{Cloud Native}-Ansatzes betrachtet.

\section{Cloud Native}
\textit{Cloud Native} beschreibt ein Paradigma, in dem sowohl technisch, organisatorisch als auch kulturell auf den optimalen Betrieb in einer \textit{Cloud Computing}-Umgebung hingearbeitet wird. Die \gls{cncf}\footnote{\citeurl{CNCF}} -- ein von der Linux Foundation gestartetes Projekt, welches Entwicklungen hinsichtlich \textit{Cloud Native}-Technologien überwacht und koordiniert\footnote{Eine Übersicht aller \gls{cncf}-Projekte ist hier zu finden \citeurl{CNCFLandscape}} -- definiert \textit{Cloud Native} wie folgt:
\blockcquote[][]{cncfDef}{\enquote{Cloud native technologies empower organizations to build and run scalable applications in modern, dynamic environments such as public, private, and hybrid clouds. [...] These techniques enable loosely coupled systems that are resilient, manageable, and observable. Combined with robust automation, they allow engineers to make high-impact changes frequently and predictably with minimal toil. [...]}}
Die Motivation zur Anwendung eines solchen Paradigmas liegt in einer schnelleren Entwicklung und Bereitstellung von Software, einer erhöhten Skalierbarkeit und Leistungsfähigkeit sowie einer verbesserten Verwaltung komplexer verteilter Systeme \cite[S. 1-11]{reznik2019cloud}. Software, die nach dem \textit{Cloud Native}-Paradigma konstruiert wurde, ist von Natur aus verteilt und als Menge von lose gekoppelten \textit{Services} realisiert. Solche \textit{Services} sind per Design schnell, robust und hochgradig skalierbar, nur \textit{serverless} bereitgestellte Anwendungen können dieses Maß an Skalierbarkeit übertreffen \cite[S. 249-250, 286-289]{reznik2019cloud}.

\section{Systemkontext}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{figures/systemkontext.pdf}
    \caption{Zu betrachtender Systemkontext}
    \label{fig:sk}
\end{figure}

Im Rahmen dieser Arbeit beschränken wir uns zunächst auf die Betrachtung einer fiktiven Organisation mit jeweils unterschiedlichen Organisationseinheiten (siehe Abbildung \ref{fig:sk}). Die jeweiligen Organisationseinheiten sind mit Computern ausgestattet, die sich im organisationseigenen Netzwerk befinden. Des Weiteren existiert die Möglichkeit, für jeden Bereich einen (physischen oder virtuellen) Server bereitzustellen. Die Bereitstellung des Servers muss nicht lokal in dem jeweiligen Bereich erfolgen, sondern kann in einem zentralen Rechenzentrum stattfinden. Geräte außerhalb des Netzwerks können über \gls{vpn} Zugang zum Organisationsnetzwerk erlangen. Das Organisationsnetzwerk kann Teil einer Cloud sein. Verteilte Nutzungskontrolle und Provenance Tracking meint in diesem Kontext also die Verteilung auf unterschiedliche Bereiche innerhalb einer logischen Organisation. Der Fall einer organisationsübergreifenden Nutzungskontrolle und Provenance Trackings wird im späteren Verlauf der Arbeit diskutiert. Ferner wird angenommen, dass auf den jeweiligen Geräten (inlusive Server) Kubernetes installiert und entsprechend konfiguriert wurde. Die Geräte bilden also einen Kubernetes-Cluster.

\section{Kubernetes} \label{sec:refkub}

Kubernetes (abgekürzt K8s) ist eine Open Source-Plattform zur Orchestrierung von containerbasierten Anwendungen. Ein Container-Orchestrator ist ein Dienst, der die Aufgaben \textit{Scheduling, Orchestration} und \textit{Cluster Management} übernimmt. Als \textit{Scheduling} wird das Management vorhandener Ressourcen und die Zuweisung von Arbeitslasten auf \textit{Nodes} im Cluster bezeichnet. \textit{Orchestration} ist die Koordinierung von Aktivitäten und Abläufen im Cluster und \textit{Cluster Management} meint das Verwalten physischer oder virtueller \textit{Nodes} \cite[S. 10-11]{arundel2019cloud}.

\subsection{Hintergrund}
Google begann bereits sehr früh damit, seine eigenen Dienste containerbasiert bereitzustellen. Zur Orchestrierung wurde ein System mit dem Namen Borg entwickelt. Dieses System war jedoch stark an Googles Interna und proprietäre Technologien gekoppelt, sodass nachträgliche Erweiterungen oder die Veröffentlichung des Quellcodes ausgeschlossen waren. Im Zuge dessen versuchte man die jahrelang gesammelten Erfahrungen der Containerorchestrierung mit Borg und die Ideen sowie Praktiken der Open Source Community in ein neues, quelloffenes Projekt einfließen zu lassen: Kubernetes \cite[S. 1-14]{arundel2019cloud}.\footnote{\citeurl{Kubernetes}}\footnote{Die Bezeichnung Kubernetes stammt aus dem griechischen und bedeutet soviel wie \enquote{Kapitän}}

Kubernetes war das erste Projekt das Teil der \gls{cncf} wurde. In den letzten Jahren hat Kubernetes den Status eines Industriestandards angenommen und bildet das Fundament für Plattformen wie OpenShift von RedHat. Jeder große \gls{csp} bietet mittlerweile Kubernetes als \textit{Managed Service} an. In diesem Kontext wird Kubernetes oft als \textit{Portability Layer} bezeichnet, da sich die Kubernetes-\gls{api} -- abgesehen von unterschiedlichen Versionen -- zwischen den \glspl{csp} nicht ändert \cite[S. 13-17]{scholl2019cloud}.

\subsection{Architektur}
Kubernetes basiert auf einer ressourcenbasierten deklarativen \gls{api} \cite[S. 175]{ibryam2020kubernetes}. Vereinfacht ausgedrückt beschreibt man mithilfe von YAML-Dateien (Manifeste gennant) Kubernetes-Objekte, die den gewünschten Systemzustand beschreiben. Kubernetes versucht mithilfe des sogennanten \textit{Reconciliation Loop}, den aktuellen Zustand an den gewünschten anzugleichen. Die deklarative Beschreibung eines Systemzustandes erleichtert in enormen Maße das Konfigurationsmanagement. Zum einen können die Manifeste in die Versionskontrolle eingebucht werden, zum anderen kann der Systemzustand einfacher überblickt werden. Eine imperative Beschreibung würde den Systemzustand als Folge mehrerer Zustandsübergänge beschreiben, im Gegensatz zur deklarativen Beschreibung \cite[S. 1-11]{burns2019kubernetes}.

Ein Kubernetes-Cluster setzt sich aus einer Menge an Computern, auch \textit{Nodes} genannt, zusammen. Ein Cluster besteht aus mindestens einem \textit{Node}. Diese unterteilen sich in \textit{Master Nodes} und \textit{Worker Nodes}. \textit{Worker Nodes} dienen als Host für \textit{Pods}, welche aus einem oder mehreren Containern bestehen. Ein \textit{Pod} ist die kleinste Einheit, die mit Kubernetes bereitgestellt werden kann. Dies hat zur Folge, dass alle Container eines \textit{Pods} immer auch auf demselben \textit{Node} bereitgestellt werden \cite[S. 1-11]{ibryam2020kubernetes}.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/kubernetes-components.pdf}
    \caption{Zentrale Komponenten eines Kubernetes-Clusters}
    \label{fig:kub}
\end{figure}

Die \textit{Master Nodes} eines Clusters bilden dessen \textit{Control Plane} (siehe Abbildung \ref{fig:kub}). Die Aufgabe der \textit{Control Plane} ist, Entscheidungen für den gesamten Cluster zu treffen und auf Events im Cluster zu reagieren. Der \texttt{kube-apiserver} ist die Schnittstelle zur Kubernetes-\gls{api} und dessen Ressourcen. \texttt{etcd} ist ein verteilter \textit{Key Value Store} mit starken Konsistenzbedingungen, in der alle für die Kubernetes-\gls{api} relevanten Daten liegen. Der \texttt{kube-scheduler} ist für die Zuweisung von \textit{Pods} auf \textit{Nodes} zuständig. \textit{Controller} sind Prozesse, die kontinuierlich den Zustand von Kubernetes-Ressourcen überwachen und Anpassungen zur Erreichung des Soll-Zustandes vornehmen. Der \texttt{kube-controller-manager} verwaltet eine Reihe solcher \textit{Controller}. Der \texttt{cloud-controller-manager} dient zur Interaktion mit der \gls{api} eines \gls{csp}. Als Beispiel einer solchen Interaktion lässt sich die automatische Bereitstellung von \textit{Nodes} im Falle eines Ressourcenengpasses nennen (\textit{Autoscaling}). Auf jedem \textit{Node}, egal ob \textit{Master} oder \textit{Worker}, läuft \texttt{kubelet}, das die Ausführung der auf diesen \textit{Node} zugewiesen \textit{Pods} sicherstellt und als Schnittstelle zur \textit{Control Plane} fungiert. Die Container eines \textit{Pods} werden über eine entsprechende \textit{Container Runtime} betrieben. Damit Kubernetes nicht an spezifische Laufzeitumgebungen gebunden ist, wurde eine einheitliche Schnittstelle, das \textit{Container Runtime Interface}, geschaffen, welches von Container Laufzeitumgebungen, wie Docker oder containerd, implementiert wird. \texttt{kube-proxy} ist ein Netzwerk-Proxy, der die Kommunikation zwischen einzelnen Komponenten im Cluster, wie \textit{Nodes} und \textit{Pods}, ermöglicht \cite[S. 33-35]{arundel2019cloud}.

\section{Modellierung und Implementierung}
Für die in Abschnitt \ref{sec:refucon} und \ref{sec:prov} vorgestellte Referenzarchitektur existieren bereits prototypische Implementierungen. Eine genaue Betrachtung der Schnittstellen sowie Beschreibungen über deren Kommunikationsverhalten und Aufgaben, führt zu folgendem Diagramm:\footnote{Komponenten, die Pfeile auf sich selbst besitzen, stehen in Beziehungen zu denselben Komponenten, jedoch in anderen Domänen}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{figures/old-interface-communication-crop.pdf}
    \caption{Diagramm zur Veranschaulichung der Beziehungen zwischen den Komponenten der Referenzarchitektur}
    \label{fig:komold}
\end{figure}

Hierbei fallen zwei Dinge auf:
\begin{inparaenum}[(i)]
    \item Der \gls{pmp} kennt jede Komponente.
    \item Die Beziehungen zwischen den Komponenten entsprechen nicht genau denen der Referenzarchitektur, sie sind wesentlich komplexer und teils bidirektional.
\end{inparaenum}

Punkt (ii) wird in der nachfolgenden Betrachtung weitestgehend ausgeklammert. Der Grund ist, dass die Änderungsvorschläge für Punkt (i) unabhängig von Punkt (ii) durchgeführt werden können. Außerdem handelt es sich bei Punkt (ii) um ein Problem, bei dem man ohne entsprechendes Domänenwissen und detaillierten Anforderungen nur schwer gehaltvolle Aussagen treffen kann.

\subsection{Modellierung}
Die in diesem Abschnitt gezeigte Modellierung ist in Anlehnung an die \textit{Cloud Native Maturity Matrix} in \cite[S. 63-84]{reznik2019cloud} entstanden (siehe hierzu Abbildung \ref{fig:cloudmaturity} im Anhang). Die Matrix beschreibt, wie unterschiedliche Prozesse, zum Beispiel das Wasserfallmodell oder Agile, Auswirkungen auf Architektur, Infrastruktur etc. haben können.

\subsubsection{Stufe I: Die Grundlagen}
In Stufe I wird die Modellierung der Problemstellung mithilfe von Kubernetes-eigenen Objekten angestrebt. Das Ergebnis ist in Abbildung \ref{fig:st1} zu sehen.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{figures/level-1-maturity-crop.pdf}
    \caption{Stufe I -- Die Grundlage bildet Kubernetes}
    \label{fig:st1}
\end{figure}
Der Kubernetes-Cluster besteht aus drei \textit{Namespaces}, \enquote{Organization}, \enquote{Division \#1} und \enquote{Division \#2}. Die Zuordnung der Komponenten orientiert sich an der Referenzarchitektur, in der alle Komponenten, außer der \gls{procp} und \gls{prp}, immer Teil einer bestimmten Domäne sind, in unserem Fall der Domänen \enquote{Division \#1} und \enquote{Division \#2}. \textit{Namespaces} sind in Kubernetes mit \textit{Views} in Datenbanken vergleichbar. Das heißt, dass die in der Abbildung gezeigte Trennung vorerst rein logischer Natur ist und damit keinerlei Aussagen über die physische Platzierung der Komponenten macht.

Jeder der aus der Referenzarchitektur bekannten Komponenten stellt einen \textit{Pod} beziehungsweise ein \textit{Deployment} dar. Ein \textit{Deployment} ist ein Kubernetes-eigenes Objekt, das die Verwaltung von \textit{Pods} auf einer höheren Abstraktionsstufe ermöglicht. Die meisten dieser \textit{Deployments} sind Teil eines \textit{Service}, welche eine Menge von \textit{Pods} gruppiert und ihnen einen festen Endpunkt vorgibt. Als Beispiel lässt sich ein \textit{Service} mit dem Namen \texttt{pdp-service} im \textit{Namespace} \enquote{Division \#1} anführen. Die Zuordnung, welche \textit{Pods} zu welchem \textit{Service} gehören, geschieht über Labels und entsprechende Selektoren. Kubernetes konfiguriert nun die \texttt{kube-proxy}-Komponente eines jeden \textit{Nodes} und sorgt dafür, dass der \textit{Service} über den \gls{url} \texttt{<service-name>.<namespace>} erreichbar ist, in unserem Fall also \texttt{pdp-service.division-1}.\footnote{Das Doppelkreuz stellt keinen gültigen Teil des Hostnamen dar und wurde deshalb weggelassen}\footnote{<namespace> könnte weggelassen werden, sollte sich die Komponente im selben \textit{Namespace} befinden} Diesen Mechanismus nennt man auch \textit{Service Discovery}. Kubernetes erlaubt \textit{Service Discovery} über \gls{dns}, wie gerade beschrieben, als auch über Umgebungsvariablen sowie manuelles Setzen von \textit{Endpoints}. Die Methode über \gls{dns} wird jedoch explizit empfohlen. Es ist auch möglich, Komponenten direkt anzusprechen, ohne dass diese Teil eines \textit{Service} sind. Die Tatsache, dass \textit{Service Discovery} in Kubernetes bereits integriert ist, macht den \gls{pmp} unnötig. Ibryam \& Huß beschreiben diesen Umstand wie folgt:
\SetBlockThreshold{1}
\blockcquote[][S. 100]{ibryam2020kubernetes}{\enquote{In the \enquote{Post Kubernetes Era}, many of the nonfunctional responsibilities of distributed systems such as placement, health check, healing, and resource isolation are moving into the platform, and so is Service Discovery and load balancing.}}
Die \glspl{pep} unterliegen der Annahme, Teil des Kubernetes-Cluster zu sein, theoretisch muss das nicht so sein. Man könnte sich mobile Geräte wie Laptops vorstellen, die nicht Teil des Clusters sind. Um diesen Geräten Zugriff zum Cluster zu geben, könnte ein Gateway etabliert werden (siehe hierzu Abbildung \ref{fig:st1g} im Anhang). An dieser Stelle sei noch einmal angemerkt, dass ein \gls{pep} auch aus einer Kombination von \gls{pep}, \gls{pip} und \gls{pxp} bestehen könnte. Diese Kombination könnte ein \textit{Pod} mit den entsprechend containerisierten Komponenten sein, welche über \gls{ipc} miteinander kommunizieren.

\subsubsection{Stufe II: Zustände auslagern}
\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{figures/level-2-maturity-crop.pdf}
    \caption{Stufe II -- Nutzung von \textit{Replicas}}
    \label{fig:st2}
\end{figure}
Einer der Kernprinzipien bei der Entwicklung von \textit{Cloud Native}-Anwendungen ist \textit{Operational Excellence}. In \cite[S. 29-34]{scholl2019cloud} fallen hierzu die Schlüsselwörter \textit{Automate Everything, Monitor Everything, Document Everything} und \textit{Design for failure}. In dieser Stufe (siehe Abbildung \ref{fig:st2}), wird versucht, die Zustände der einzelnen Komponenten weitestgehend auszulagern. Durch Auslagerung der Zustände können mehrere Instanzen derselben Komponente gestartet und als ein gemeinsamer \textit{Service} angesprochen werden. In Stufe I war die Architektur sehr anfällig gegenüber eines Ausfalls einzelner Komponenten. Ein Ausfall muss dabei nicht einmal auf einen Fehler zurückzuführen sein. Bereits die Aktualisierung einer Komponente hätte verheerende Folgen für den gesamten Betrieb. Bei einer enstprechend guten Verteilung der \textit{Pods} auf möglichst unterschiedliche \textit{Nodes}, könnten sogar einzelne \textit{Nodes} ausfallen, ohne dabei den Betrieb ernsthaft zu gefährden. In diesem Kontext spricht man auch von einem System, welches eine \textit{Zero Downtime} ermöglicht. Selbst bei Aktualisierung einzelner Komponenten ist es mit Kubernetes möglich, den Betrieb weiterhin aufrechtzuhalten. Im Anhang (siehe Abbildung \ref{fig:st2g}) befindet sich noch einmal die Abbildung \ref{fig:st2} mit entsprechendem Gateway für externe \glspl{pep}.

\subsubsection{Stufe III und IV: Weitere Ergänzungen}
Die zwei nächsten Stufen greifen weitere Verbesserungsvorschläge auf. Stufe III (siehe Abbildung \ref{fig:st3}) stellt die Frage, ob eine Aufteilung des Kubernetes-Cluster nach Organisationseinheiten sinnvoll erscheint, da die Zustände nun ausgelagert sind. Hierbei lässt sich argumentieren, dass die Auftrennung in Organisationseinheiten erst bei einer entsprechenden Größe, wie Mutterkonzern und Tochterkonzern sinnvoll ist, da in diesem Fall auch eine entsprechend physisch aufgeteilte Infrastruktur vorhanden sein dürfte. Sofern Regulierungen es nicht anders vorschreiben, könnte darauf verzichtet werden. Der Autor Bier \cite{Bier2017} hatte die hierarchische Architektur etabliert, um Daten möglichst lokal zu halten und verwalten zu können. In der Praxis dürften Unternehmen jedoch daran interessiert sein, Rechenkapazität zu zentralisieren, um dadurch Kosten einsparen zu können. Ein hierarchisches System sorgt in diesem Fall nur für zusätzliche Komplexität.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/level-3-maturity-crop.pdf}
    \caption{Stufe III -- Entfernung der Namespaces}
    \label{fig:st3}
\end{figure}

Stufe IV sieht die Einführung eines \textit{Service Meshes} vor. Ein \textit{Service Mesh} ergänzt den Kubernetes-Cluster um zusätzliche Funktionalität auf \textit{Service}-Ebene, wie \textit{Traffic Management, Failure Handling, Security, Tracing} und \textit{Monitoring} \cite[S. 59-72]{scholl2019cloud}. Das Thema \textit{Service Mesh} ist jedoch sehr umfrangreich und soll an dieser Stelle nicht weiter vertieft werden.

\subsubsection{Stufe V: Eventbasiert und Serverless}
\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/level-5-maturity-crop.pdf}
    \caption{Stufe V -- Umsetzung einer eventbasierten \textit{Serverless}-Architektur}
    \label{fig:st5}
\end{figure}
In Stufe V wird weitestgehend von der physischen Infrastruktur abstrahiert. Die zentrale Komponente dieser Architektur ist eine Event-Queue, in Abbildung \ref{fig:st5} als \enquote{Pub/Sub} bezeichnet, die den Start von \textit{serverless} betriebenen Containern anstößt. Die Event-Queue funktioniert nach dem \textit{Publish-Subscribe Pattern}.  Komponenten schicken Events an Kanäle, \textit{Topics} genannt. Diese Events werden daraufhin von anderen Komponenten bearbeitet, die an spezifische \textit{Topics} vorher \textit{subscribed} haben. In unserem Fall löst ein solches Event jedoch automatisch den Start eines Containers beziehungsweise einer Funktion aus. Diese nimmt das Event aus der Event-Queue, bearbeitet das Event, generiert hierbei eventuell weitere Events und beendet sich wieder. Die Kernmerkmale dieser Architektur sind eine sehr hohe Skalierbarkeit, Fehlertoleranz sowie effiziente Nutzung der vorhandenen Ressourcen. Anstelle von Prozessen, die über eine lange Zeit laufen müssen, kann hierbei die Ausführung bedarfsgerecht erfolgen, das Ergebnis ist maximale Elastizität. Die Event-Queue selbst kann ein verteiltes System sein, wie zum Beispiel Apache Kafka.\footnote{\citeurl{Kafka}}

\subsection{Implementierung}
Die Implementierung der Stufen I-III wurde mithilfe von Kubernetes und \textit{Dummy}-Komponen-ten realisiert.\footnote{Das zugehörige Repository ist hier zu finden: \citeurl{GitLab}} Jeder der Komponenten wurde in Python programmiert und steht unter der \textit{MIT-License}. Als Web-Framework wurde FastAPI verwendet.\footnote{\citeurl{fastapi}} Mithilfe der GitLab CI/CD wird bei jedem \textit{Push} automatisiert die Einhaltung des \textit{Code Styles} überprüft\footnote{Verwendet wurde der black \textit{Code Style} \citeurl{black}}, Tests ausgeführt (außer beim \gls{pep}) und das entsprechende Docker Image gebaut und auf DockerHub hochgeladen.\footnote{\citeurl{DockerHub}} Aus Gründen der Übersicht wurden alle Images auf dasselbe Docker-Repository hochgeladen und lediglich entsprechend getagged. Damit die Container ordnungsgemäß gestartet werden können, müssen mehrere Umgebungsvariablen beim Start des Containers übergeben werden. Dies geschieht automatisiert durch Kubernetes. Ein Großteil der Variablen wird durch die \textit{Downward API} von Kubernetes injiziert, welche die Umgebungsvariablen dynamisch erzeugt. Als Beispiel sei hierfür die Umgebungsvariable mit den Namen des \textit{Nodes} genannt, auf dem der Container beziehungsweise \textit{Pod} ausgeführt wird.

Die \glspl{pep} werden als \textit{DaemonSet} gestartet. Dies hat zur Folge, dass auf jedem Node ein \gls{pep} platziert wird. Die \glspl{pep} versuchen daraufhin, über den Websocket-Endpunkt \texttt{/ws} eine Verbindung zu einem \gls{pdp} und \gls{pip} aufzubauen. Der genaue Kommunikationsablauf kann aus Abbildung \ref{fig:seq} entnommen werden. Die \texttt{par} Box stellt eine parallele Ausführung dar. Die \textit{Dummy}-Komponten verwenden hierfür \texttt{asyncio} welches einen asynchronen Kommunikationsablauf ermöglicht. Der Grund für die Nutzung von Websockets war das Etablieren einer bidirektionalen Verbindung. Nachrichten können ohne erneuten Verbindungsaufbau vom \gls{pep} empfangen und gesendet werden. Die anderen Komponenten starten einen Webserver, welcher die in Tabelle \ref{tab:endpoints} dargestellten Endpunkte bereitstellt. Jedoch stellen nur \gls{pdp}, \gls{pip} und \gls{prosp} den Endpunkt \texttt{/ws} zur Verfügung.
Zur Visualisierung sind im Anhang OpenAPI-Spezifikation sowie dazugehörige JSON-Objekte mit beispielhaften Werten angegeben.
\renewcommand*{\thefootnote}{\fnsymbol{footnote}}
\begin{table}
    \centering
    \small
    \begin{tabular}{ c l } \toprule
        \bfseries{Endpunkt} & \bfseries{Beschreibung}                                          \\ \midrule
        /event              & Zum Senden eines Events an die Komponente\footnotemark[2]       \\
        /info               & Informationen über die Komponente\footnotemark[2]               \\
        /ws                 & Endpunkt, mit dem eine Websocket-Verbindung aufgebaut werden kann \\
        /docs               & Aufzeigen der OpenAPI-Spezifikation mithilfe der Swagger UI      \\
        /redoc              & Aufzeigen der OpenAPI-Spezifikation mithilfe von ReDoc           \\
        \hline
    \end{tabular}
    \caption{Zur Verfügung stehende Endpunkte der \textit{Dummy}-Komponenten}
    \label{tab:endpoints}
\end{table}
\footnotetext[2]{Siehe hierzu die JSON-Objekte und OpenAPI-Spezifikation im Anhang}
\renewcommand*{\thefootnote}{\arabic{footnote}}
\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.98\textwidth]{figures/readability/sequenzdiagramm.pdf}
    \caption{Sequenzdiagramm zur Kommunikation von \gls{pep}, \gls{pdp} und \gls{pip}}
    \label{fig:seq}
\end{figure}

Eine der Anforderungen war es, genauer bestimmen zu können, wo \textit{Pods} \textit{scheduled} werden. Wie bereits angesprochen handelt es sich bei der Unterteilung in \textit{Namespaces} lediglich um eine rein logische, welche keinen Einfluss auf die Platzierung der \textit{Pods} auf \textit{Nodes} hat. Es existieren zwar Plugins, mit denen dies möglich ist (siehe hierzu \textit{PodNodeSelector}), aber diese sind standardmäßig deaktiviert und bei \textit{Managed Cluster}, wie Google's GKE, nicht aktivierbar. Mit den in Kubernetes standardmäßig vorhandenen Mitteln lassen sich die in Abbildung \ref{fig:select} gezeigten Attribute selektieren. Dazu gehören \textit{Pools} von \textit{Nodes}, Namen von \textit{Nodes} sowie Labels, mit denen die \textit{Nodes} versehen wurden.\footnote{Kubernetes erlaubt die Zuweisung von Labels auf fast alle Objekte} Die Selektion über den Namen eines \textit{Nodes} wird jedoch offiziell nicht empfohlen, weshalb die beiden anderen Möglichkeiten benutzt wurden. Die Selektion geschieht über den \texttt{nodeSelector}. Dies wurde sowohl lokal mithilfe von minikube\footnote{\citeurl{minikube}}, als auch in der Google Cloud mit GKE getestet.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.55\textwidth]{figures/selectable-crop.pdf}
    \caption{Selektierbare Eigenschaften in Kubernetes}
    \label{fig:select}
\end{figure}

Hierbei zeigte sich, dass die manuelle Erstellung der Kubernetes-Manifeste für komplexere Szenarien, wie mehrere Organisationseinheiten, mit sehr viel Redundanz einhergeht. Diese Redundanz ist potentiell anfällig für Fehler, sollten Änderungen an manchen Stellen vergessen werden. Um dem entgegenzuwirken und den Prozess zur Erstellung der Kubernetes-Manifeste zu automatisieren, existiert Helm.\footnote{\citeurl{Helm}} Helm bezeichnet sich selbst als Paketmanager für Kubernetes. Die Grundidee hinter Helm ist, mithilfe von Templates die Kubernetes Manifeste zu beschreiben und die konkrete Konfiguration an zentralen Stellen wie YAML-Dateien auszulagern. Die Templates können dann gerendert und die daraus erzeugten Manifeste im Kubernetes-Cluster \textit{deployed} werden. Das templating wird mit Go-Templates und weiteren ergänzenden Funktionen realisiert. Templating ist ein sehr mächtiges Werkzeug, da hiermit Schleifen und weitere Programmierkonstrukte für die Erstellung der Manifeste nutzbar sind. Die Beschreibung der Konfiguration geschieht bei Helm im YAML-Format. YAML ist eine Obermenge von JSON und ideal zur Konfigurationsbeschreibung geeignet, unter anderem deswegen, da Konfigurationen anhand von Schemata validierbar sind. Ein Beispiel für eine solche Konfigurationsbeschreibung ist in Listing \ref{fig:lyaml} zu sehen. Darin wird eine Organisation mit drei Organisationseinheiten beschrieben. Mithilfe vorbereiteter Templates können daraus konkrete Kubernetes-Manifeste erzeugt werden, die dann zur Bereitstellung der Nutzungskontroll- und Provenance Tracking-Infrastruktur genutzt werden können. Die Templates für obiges Beispiel sind im Ordner \texttt{helm-package/ucon-prov/templates} des \texttt{kubernetes}-Repository in GitLab zu finden. Ausschnitte des Quellcodes befinden sich am Ende des Anhangs.
\begin{code}
\centering
\begin{pyglist}[language=yaml,bgcolor=codeGrey,fontsize=\small]
divisions:
  - name: "Research and Development"
    namespace: "research"
    description: "Department of development. Person in charge is Sundar Pichai."
    [...]
  - name: "Accounting"
    namespace: "accounting"
    description: "Department of accounting. Person in charge is Ruth Porat."
    [...]
  - name: "Marketing"
    namespace: "marketing"
    description: "Department of marketing. Person in charge is Tim Cook."
    [...]
\end{pyglist}
\caption{Beispiel einer Konfigurationsbeschreibung mit YAML}
\label{fig:lyaml}
\end{code}

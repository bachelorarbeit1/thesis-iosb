# Bachelorarbeit: Verteilte Nutzungskontrolle und Provenance Tracking am Beispiel von Cloud-Technologien, Nicolas Schuler

Übersicht:

- [Zeitplan](#zeitplan)
- [Glossar](#glossar)

## Zeitplan

| Aufgabe                                                      | Dauer     | Deadline | Geschätzte Seitenzahl |
| ------------------------------------------------------------ | --------- | -------- | --------------------- |
| Grundlagen - Theoretische Grundlagen                         | 1 Wochen  | 2.09     | 6-7                   |
| Grundlagen - Aktuelle Forschung / Forschungsprojekte         | 1-2 Woche | 16.09    | 10                    |
| Cloud Computing - (Def, Service Modelle, Deployment Modelle) | 1 Woche   | 23.09    | 2-3                   |
| Cloud Computing - Implikationen, Sicherheit                  | 2-3 Woche | 14.10    | 6-8                   |
| Nutzungskontrolle und Provenance Tracking in der Cloud       | 4 Wochen  | 11.11    | 9-14                  |
| Evaluation                                                   | 2 Wochen  | 25.11    | 4                     |
| Zusammenfassung und Ausblick                                 | 1 Woche   | 02.12    | 3                     |
| Korrekturlesen                                               | 1 Woche   | 09.12    | -                     |
| Einleitung                                                   | 1 Woche   | 16.12    | 3                     |
| Abgabe                                                       |           |          |                       |
| Präsentation                                                 |           |          |                       |

## Glossar

- [Glossar/Ubiquitous Language](https://gitlab.com/bachelorarbeit1/thesis-iosb/-/blob/master/ubiquitous_language.md)
